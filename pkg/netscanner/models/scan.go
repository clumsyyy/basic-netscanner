package models

type Scan struct {
	ScanId    int64
	Timestamp int64
	Hosts     map[int64]Host
}

type Host struct {
	HostId   int64
	Scans    map[int64]Scan
	TcpPorts map[int64]TcpPort
	UdpPorts map[int64]UdpPort
}

type TcpPort struct {
	TcpPortId int64
	Number    int32
}

type UdpPort struct {
	UdpPortId int64
	Number    int32
}
