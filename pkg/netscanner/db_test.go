package main

import (
	"testing"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

var (
	NUM_TABLES = 7
)

func Test_Db_create_and_reset(t *testing.T) {
	t.Run("test_create_and_reset_on_db", func(t *testing.T) {
		// Create the database and run the schema query to create tables
		db, err := sqlx.Connect("sqlite3", ":memory:")
		if err != nil {
			t.Fatalf("unable to create in-memory sqlite db, err = %s", err.Error())
		}
		db.MustExec(BuildSchema())

		// Count the number of tables
		rows, err := db.Queryx("SELECT name FROM sqlite_master WHERE type='table';")
		if err != nil {
			t.Fatalf("unable to run sqlite_master select, err = %s", err.Error())
		}
		counter := 0
		for rows.Next() {
			counter++
		}

		// Check number of tables and fail if wrong
		if counter != NUM_TABLES {
			t.Fatalf(
				"After creating schema expected %d tables, have %d tables",
				NUM_TABLES, counter,
			)
		}

		// Run query to delete tables
		db.MustExec(DbReset())

		// Rerun query to list all tables
		rows, err = db.Queryx("SELECT name FROM sqlite_master WHERE type='table';")
		if err != nil {
			t.Fatalf("unable to run sqlite_master select, err = %s", err.Error())
		}
		counter = 0
		for rows.Next() {
			counter++
		}
		if counter > 0 {
			t.Fatalf("After deleting schema expected 0 tables, have %d tables", counter)
		}
	})
}
