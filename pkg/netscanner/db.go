package main

import (
	"strings"
)

func BuildSchema() string {
	var sb strings.Builder
	sb.WriteString(dbSchema)
	return sb.String()
}

func DbReset() string {
	var s = `
		DROP TABLE IF EXISTS scans;
		DROP TABLE IF EXISTS hosts;
		DROP TABLE IF EXISTS tcp_ports;
		DROP TABLE IF EXISTS udp_ports;
		DROP TABLE IF EXISTS scans_hosts_join;
		DROP TABLE IF EXISTS hosts_tcp_ports_join;
		DROP TABLE IF EXISTS hosts_udp_ports_join;
	`
	return s
}

var dbSchema string = `
CREATE TABLE IF NOT EXISTS scans (
	scan_id INTEGER PRIMARY KEY NOT NULL,
	timestamp INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS hosts (
	host_id INTEGER PRIMARY KEY NOT NULL,
	ip TEXT NOT NULL,
	hostname TEXT
);

CREATE TABLE IF NOT EXISTS tcp_ports (
	tcp_port_id INTEGER PRIMARY KEY NOT NULL,
	number INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS udp_ports (
	udp_port_id INTEGER PRIMARY KEY NOT NULL,
	number INTEGER NOT NULL
);

CREATE TABLE IF NOT EXISTS hosts_tcp_ports_join (
	hosts_tcp_ports_join_id INTEGER PRIMARY KEY NOT NULL,
	host_id INTEGER NOT NULL,
	tcp_port_id INTEGER NOT NULL,
	FOREIGN KEY (tcp_port_id) REFERENCES tcp_ports(tcp_port_id)
);

CREATE TABLE IF NOT EXISTS hosts_udp_ports_join (
	hosts_udp_ports_join_id INTEGER PRIMARY KEY NOT NULL,
	host_id INTEGER NOT NULL,
	udp_port_id INTEGER NOT NULL,
	FOREIGN KEY (udp_port_id) REFERENCES udp_ports(udp_port_id)
);

CREATE TABLE IF NOT EXISTS scans_hosts_join (
	scans_hosts_join_id INTEGER PRIMARY KEY NOT NULL,
	scan_id INTEGER NOT NULL,
	host_id INTEGER NOT NULL,
	FOREIGN KEY (scan_id) REFERENCES scans(scan_id),
	FOREIGN KEY (host_id) REFERENCES hosts(host_id)
);
`

var InsertScan string = "INSERT INTO scans (timestamp) VALUES (unixepoch());"
