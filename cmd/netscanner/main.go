package main

import (
	"flag"
	"log"
	"net"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"

	"gitlab.com/clumsyyy/basic-netscanner/pkg/netscanner"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lmicroseconds | log.Lshortfile)

	var RESET_DB bool
	flag.BoolVar(&RESET_DB, "resetdb", false, "reset the db")
	flag.Parse()

	// set up db
	db, err := sqlx.Connect("sqlite3", "file:sqlite.db?cache=shared")
	if err != nil {
		log.Fatalf("db connection failed, %s", err.Error())
	}

	// Will crash if fails
	if RESET_DB {
		runDbReset(db)
	}

	// ensure tables are present
	runTableSetup(db)

	Scanner(db)
}

func Scanner(db *sqlx.DB) {
	//client := net.Dialer{Timeout: time.Second * 5}

	start := time.Now().Unix()

	result, err := db.Exec(nestscanner.InsertScan)
	if err != nil {
		log.Fatalf("Unable to run query |%s|, err = %s", netscanner.InsertScan, err.Error())
	}
	scanId, err := result.LastInsertId()
	if err != nil {
		log.Fatalf("result.LastInsertId() returned err = %s", err.Error())
	}

	log.Printf("Starting scan %d", scanId)

	/*
		hosts := []string{"10.0.1.2"}

		for _, host := range hosts {
			hasOpenPort := false
			for i := 0; i < 40000; i++ {
				tcpChan := make(chan bool)
				udpChan := make(chan bool)

				host += fmt.Sprintf(":%d", i)

				go tcpConnect(&client, "tcp", host, tcpChan)
				go udpConnect(&client, "udp", host, udpChan)

				tcpRes := <-tcpChan
				if tcpRes {
					hasOpenPort = true
				}

				udpRes := <-udpChan
				if udpRes {
					hasOpenPort = true
				}
			}

			if hasOpenPort {
				// create a new host row here
			}
		}
	*/
	log.Printf("Elapsed %d seconds", time.Now().Unix()-start)
}

func tcpConnect(client *net.Dialer, protocol, addr string, c chan bool) {
	_, err := client.Dial(protocol, addr)
	if err != nil {
		//log.Printf("error dialing %s\n", addr)
		c <- false
	}
	c <- true
}

func udpConnect(client *net.Dialer, protocol, addr string, c chan bool) {
	_, err := client.Dial(protocol, addr)
	if err != nil {
		//log.Printf("error dialing %s\n", addr)
		c <- false
	}
	c <- true
}

func runTableSetup(db *sqlx.DB) {
	res := db.MustExec(BuildSchema())
	_, err := res.LastInsertId()
	if err != nil {
		log.Fatalf("BuildSchema() result.lastInsertId err = %s", err.Error())
	}
	_, err = res.RowsAffected()
	if err != nil {
		log.Fatalf("BuildSchema() result.RowsAffected() err = %s", err.Error())
	}

}

func runDbReset(db *sqlx.DB) {
	res := db.MustExec(DbReset())
	_, err := res.LastInsertId()
	if err != nil {
		log.Fatalf("reset db possibly failed, LastInsertId() error = %s", err.Error())
	}
	_, err = res.RowsAffected()
	if err != nil {
		log.Fatalf("reset db possibly failed, RowsAffected() error = %s", err.Error())
	}

}
